package com.tomando;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MiniGameAdapter extends ArrayAdapter<MiniGame> {

	private final Context context;
	
	  private MiniGame[] items;

      public MiniGameAdapter(Context context, int textViewResourceId, MiniGame[] items) {
              super(context, textViewResourceId, items);
              this.context = context;
              this.items = items;
      }

      @Override
      public View getView(int position, View convertView, ViewGroup parent) {
              View v = convertView;
              if (v == null) {
                  LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                  v = vi.inflate(R.layout.mini_game_view, null);
              }
              
              MiniGame mg = items[position];
              if (mg != null) {
                      TextView tt = (TextView) v.findViewById(R.id.toptext);
                      TextView bt = (TextView) v.findViewById(R.id.bottomtext);
                      if (tt != null) {
                            tt.setText(mg.getName());
                      }
                      if(bt != null){
                            bt.setText("Descripción: " + mg.getDescription());
                      }
                      
                      v.setTag(mg);
              }
          
              return v;
      }
}
