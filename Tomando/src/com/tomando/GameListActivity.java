/**
 * 
 */
package com.tomando;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

/**
 * @author Angel
 *
 */
public class GameListActivity extends Activity {

    private MiniGameAdapter m_adapter;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //get the message from the intent        
        Intent intent = getIntent(); 
        
        int playerQuantity = Integer.parseInt(intent.getStringExtra(MainActivity.PLAYER_QUANTITY));
        
        setContentView(R.layout.activity_game_list_main);
        
        //Get the players'turn and display message
        TextView text = (TextView) findViewById(R.id.textViewTitle);
        text.setText( "Es tu turno Borrach@ " + String.valueOf(RandomManager.randomWithMax(playerQuantity)) + "! Selecciona tu mini-juego:");  
        
        ListView listView = (ListView) findViewById(R.id.listViewMiniGame);
        
        //Get a list of mini-games
        MiniGame[] values = MiniGameManager.getMiniGames(playerQuantity);
        
        this.m_adapter = new MiniGameAdapter(this, R.layout.mini_game_view, values );
                        
        // Assign adapter to ListView
        listView.setAdapter(this.m_adapter);
        
        listView.setOnItemClickListener(new OnItemClickListener() {

			public void onItemClick(AdapterView<?> parent, View view, int position,
					long itemID) {
			
				// Do something in response to button
		    	//Intent intent = new Intent(getApplicationContext(), MiniGameDetailsActivity.class);
		    	
//		    	MiniGame mg = (MiniGame) view.getTag();
//		    	intent.putExtra("minigameid", mg.getGameId() );
//		    	
//		    	startActivity(intent);
				
			}
		});
        
	}
}
