package com.tomando;

public class MiniGame {
	private String gameId;
	private String name;
	private String description;
	private String image;
	
	public MiniGame() {
	}
	public MiniGame(String id, String name, String description, String image) {
		this.gameId = id;
		this.name = name;
		this.description = description;
		this.image = image;
	}
	
	public String getGameId() {
		return gameId;
	}
	public void setGameId(String gameId) {
		this.gameId = gameId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
}
