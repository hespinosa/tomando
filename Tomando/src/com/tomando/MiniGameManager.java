package com.tomando;
import android.util.SparseArray;

public class MiniGameManager {

	private static SparseArray<MiniGame> miniGamesDef = new SparseArray<MiniGame>(10);
	private static int totalMinigames = 0;
	
	public MiniGameManager() {
		int i = 0;
		MiniGame mg = new MiniGame("cascada"
				, "Cascada"
				, "Sea n el numero total de jugadores, de tal forma que el jugador 1 es quien cayo en el casillero, el jugador 2 es quien esta inmediatamente a la derecha del jugador 1 y as sucesivamente. El jugador 1 debe comenzar a tomar. El jugador 2 debe comenzar a tomar luego de que el jugador 1 comience. En general, el jugador m debe comenzar a tomar luego de que el jugador m  1 comience (m = 2; 3; : : : ; n). Ningun jugador puede parar de tomar mientras el jugador 1 este tomando. En cuanto el jugador 1 pare de tomar, el jugador 2 tiene la posibilidad de seguir tomando (obligando al resto a seguir tomando) o en parar de tomar, otorgando la posibilidad de elegir entre continuar o parar al jugador 3, y as sucesivamente hasta el jugador n."
				, "");
		MiniGameManager.miniGamesDef.put(i++, mg); 
		mg = new MiniGame("cultura_chupistica"
				, "Cultura Chup�stica"
				, "asdfk�ljasd askjd lk�asj klasdjf lasdjfjfjd kkjsdkjfskldjskldjf lskdljsdlf jsldjflsdjf lsdjflsj dlsdflkjsdlkjfslkdj f."
				, "");
		MiniGameManager.miniGamesDef.put(i++, mg);
		mg = new MiniGame("nunca_nunca"
				, "Nunca Nunca"
				, "asdfk�ljasd askjd lk�asj klasdjf lasdjfjfjd kkjsdkjfskldjskldjf lskdljsdlf jsldjflsdjf lsdjflsj dlsdflkjsdlkjfslkdj f."
				, "");
		MiniGameManager.miniGamesDef.put(i++, mg);
		mg = new MiniGame("tapita_puritana"
				, "Tapita Puritana"
				, "asdfk�ljasd askjd lk�asj klasdjf lasdjfjfjd kkjsdkjfskldjskldjf lskdljsdlf jsldjflsdjf lsdjflsj dlsdflkjsdlkjfslkdj f."
				, "");
		MiniGameManager.miniGamesDef.put(i++, mg);
		mg = new MiniGame("todos_toman"
				, "Todos Toman"
				, "Todos los jugadores beben 1 sorbo."
				, "");
		MiniGameManager.miniGamesDef.put(i++, mg);
		mg = new MiniGame("toma_n"
				, "Toma {N}"
				, "El jugador debe tomar {N} sorbos de su vaso."
				, "");
		MiniGameManager.miniGamesDef.put(i++, mg);
		mg = new MiniGame("toma_x"
				, "Toma {X}"
				, "Toma {X}."
				, "");
		MiniGameManager.miniGamesDef.put(i++, mg);
		mg = new MiniGame("toman_xx"
				, "Toman {XX}"
				, "Toman {XX}."
				, "");
		MiniGameManager.miniGamesDef.put(i++, mg);
		
		MiniGameManager.totalMinigames = i;
	}
		
	/**
	 * Returns a collection of <i>howMany</i> minigames definitions, randomly.
	 * @param howMany The number of minigames to return
	 * @return A collection of MiniGame's
	 */
	public static MiniGame[] getMiniGames(int howMany) {
		MiniGame[] minigames = new MiniGame[howMany];
		SparseArray<MiniGame> history = MiniGameManager.miniGamesDef.clone();
		
		for(int i = 0; i < howMany; i++) {
			System.out.println("******************************* iteracion " + String.valueOf(i));
			int index = RandomManager.randomWithMax(MiniGameManager.totalMinigames - i);
			MiniGame mg = history.get(index);
			//TODO: hacer que no se repitan
//			boolean success = i == 0 ? true : false;
//			while(!success) {
//				
//			}
			minigames[i] = mg;
			history.remove(i);
		}
		return minigames;
	}

}
