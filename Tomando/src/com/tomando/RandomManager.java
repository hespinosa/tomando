package com.tomando;

import java.util.Random;

public class RandomManager {
	
	public static int randomWithMax(int max)
	{
		Random r = new Random();
		return r.nextInt(max)+1;
	}
}
