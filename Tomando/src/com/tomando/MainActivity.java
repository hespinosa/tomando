package com.tomando;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;

public class MainActivity extends Activity {

	public final static String PLAYER_QUANTITY = "com.tomando.PLAYER_QUANTITY";
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Instantiate the MiniGameManager
        new MiniGameManager();
        setContentView(R.layout.activity_main);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    /** Called when the user selects the Send button */
    public void sendMessage(View view) {
        
    	// Do something in response to button
    	Intent intent = new Intent(this, GameListActivity.class);
    	
    	String playerQuantity = view.getTag().toString();
    	intent.putExtra(MainActivity.PLAYER_QUANTITY, playerQuantity);
    	
    	startActivity(intent);
    	
    }
    
}
